<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('laravel_database_private-conversation.{id}', function ($user, $conversationId) {
    $conversationRepository = app()->make(\App\Chat\Conversation\ConversationRepositoryInterface::class);

    $conversation = $conversationRepository->find($conversationId);

    return $conversation->creator_id === $user->id || $conversation->receiver_id === $user->id;
});


