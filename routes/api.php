<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Authentication'], function() {
    Route::post('authenticate', 'AuthenticationController@authenticate')->name('authenticate');
    Route::post('logout', 'AuthenticationController@logout')->name('logout');
});

Route::group(['namespace'=>'Registration'], function() {
    Route::post('register', 'RegistrationController@register')->name('register');
});

Route::group(['namespace'=>'Conversation','prefix'=>'conversation'], function() {
    Route::get('conversations', 'ConversationController@getConversationsWithSingleMessage')
        ->name('getConversationsWithSingleMessage');
    Route::post('create', 'ConversationController@createConversation')->name('createConversation');
    Route::get('/messages/get', 'ConversationController@getMessagesForConversationId')->name('getMessagesForConversationId');
    Route::post('/userIsTyping', 'ConversationController@userIsTyping')->name('userIsTyping');
});

Route::group(['namespace'=>'Message', 'prefix'=>'message'], function() {
    Route::post('/', 'MessageController@newMessage')->name('createNewMessage');
});
Route::group(['namespace'=>'User', 'prefix'=>'user'], function() {
    Route::get('/', 'UserController@getUsersForChat')->name('getUsersForChat');
});
Route::group(['namespace'=>'GroupMessage', 'prefix'=>'groupMessage'], function() {
    Route::get('/', 'GroupMessageController@getGroupMessages')->name('getGroupMessages');
    Route::post('/new/message', 'GroupMessageController@newGroupMessage')->name('newGroupMessage');

});
