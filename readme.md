

## About App

Couple of notes before reading...

I didnt't know how to use laravel echo to brodacst from frontend to backend using socket io
so i'm using axios with post requests to save messsages and update user is typing...
I had a lot of problems setting up docker to run with laravel-echo-server cost me more time then expected
so i'm using machine development endearment.

Also there is a integration tests but it's missing the unit tests for repositories etc...
Also apologizes i created a group chat and user to user chat miss read the instructions that multiple users can chat in a single channel
if it's totally required i will rewrite app so it supports that also that feature...
For frontend part i'm using react as simple as possible...

apologizes if frontend is not fully completed it's missing checheks if user is authenticated and code is not preatty at all
didn't use typescript to save time... 

## Requirements
- Redis
- nodejs
- npm
- php 7.2

## Setup
- ./composer.phar install
- npm install
- npm install -g laravel-echo-server
- cp .env.example .env
- php artisan migrate

## Tests

After setup is completed run tests to insure that apps run as supposed

- ./vendor/bin/phpunit

## Running App

All commands needs to be run

- php artisan serve
- laravel-echo-server start
- redis-server start
- php artisan queue:listen --tries=1
