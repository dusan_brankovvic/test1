<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('conversation_id');
            $table->foreign('conversation_id')->references('id')->on('conversations');
            $table->string('sender_id');
            $table->foreign('sender_id')->references('id')->on('users');
            $table->string('receiver_id');
            $table->foreign('receiver_id')->references('id')->on('users');
            $table->text('message')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->string('attachment_url')->nullable();
            $table->tinyInteger('delivered')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
