<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Chat\Conversation\Conversation::class, function (Faker $faker) {
    return [
        'id' => $faker->uuid,
        'creator_id' => function () use ($faker) {
            return factory(\App\User\User::class)->create()->id;
        },
        'receiver_id' => function () use ($faker) {
            return factory(\App\User\User::class)->create()->id;
        }
    ];
});
