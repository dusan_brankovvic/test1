<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Chat\Message\GroupMessage::class, function (Faker $faker) {
    return [
        'message'=>$faker->text,

        'sender_id' => function () {
            return factory(\App\User\User::class)->create()->id;
        },
        'created_at'=>\Carbon\Carbon::now(),

        'updated_at'=>\Carbon\Carbon::now()
    ];
});
