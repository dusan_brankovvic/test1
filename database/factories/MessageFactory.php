<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Chat\Message\Message::class, function (Faker $faker) {
    $type = $faker->randomElement([\App\Chat\Message\Message::$MESSAGE, \App\Chat\Message\Message::$PICTURE, \App\Chat\Message\Message::$VIDEO]);
    return [
        'conversation_id' => function () {
            return factory(\App\Chat\Conversation\Conversation::class)->create()->id;
        },
        'message' => function () use ($type, $faker) {
            if ($type === \App\Chat\Message\Message::$MESSAGE) {
                return $faker->text;
            }
        },
        'sender_id' => function () {
            return factory(\App\User\User::class)->create()->id;
        },
        'receiver_id' => function() {
            return factory(\App\User\User::class)->create()->id;
        },
        'type' => $type,
        'attachment_url' => function () use ($type, $faker) {
            if ($type !== \App\Chat\Message\Message::$MESSAGE) {
                return $faker->url;
            }
        },
        'delivered'=> $faker->randomElement([0,'1']),

        'created_at'=>\Carbon\Carbon::now(),

        'updated_at'=>\Carbon\Carbon::now()
    ];
});
