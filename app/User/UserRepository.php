<?php

namespace App\User;

use App\RepositoryAbstract;
use App\RepositoryInterface;

class UserRepository extends RepositoryAbstract implements UserRepositoryInterface
{
    protected $model = null;

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}
