<?php

namespace App\Events;

use App\Chat\Message\GroupMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GroupEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var GroupMessage
     */
    private $groupMessage;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(GroupMessage $groupMessage)
    {
        //
        $this->groupMessage = $groupMessage;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('group');
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->groupMessage
        ];
    }
}
