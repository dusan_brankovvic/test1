<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Services\Authentication\AuthenticateService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    /**
     * @var AuthenticateService
     */
    private $authenticateService;

    public function __construct(AuthenticateService $authenticateService)
    {

        $this->authenticateService = $authenticateService;
    }

    public function authenticate(AuthenticationRequest $authenticationRequest): JsonResponse
    {
        $token = $this->authenticateService->authenticate(
            $authenticationRequest->get('email'),
            $authenticationRequest->get('password')
        );

        if(is_null($token)) {
            return response()->json([], 401);
        }

        return response()->json($token, 201);
    }

    public function logout()
    {
        $this->authenticateService->logout();

        return response()->json([], 201);
    }
}
