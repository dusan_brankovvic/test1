<?php

namespace App\Http\Controllers\Conversation;

use App\Chat\Conversation\ConversationRepositoryInterface;
use App\Events\UserIsTyping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConversationController extends Controller
{
    /**
     * @var ConversationRepositoryInterface
     */
    private $conversationRepository;

    public function __construct(ConversationRepositoryInterface $conversationRepository)
    {

        $this->conversationRepository = $conversationRepository;
    }

    public function getConversationsWithSingleMessage() {
        return response()->json($this->conversationRepository->getConversationsWithFirstMessage(auth('api')->id()));
    }

    public function createConversation(ConversationRepositoryInterface $conversationRepository, Request $request) {
        $conversation = $conversationRepository->createConversation(auth()->id(), $request->json('receiver_id'));

        if(is_null($conversation)) {
            return response()->json(['conversation'=>null, 'error'=>'Failed to create conversation'], 401);
        }

        return response()->json(['conversation'=>$conversation, 'error'=>null], 201);
    }

    public function getMessagesForConversationId(Request $request) {
        $messages = $this->conversationRepository->messagesForConversationId($request->get('conversation_id'));

        return response()->json($messages);
    }

    public function userIsTyping(Request $request) {
        broadcast(new UserIsTyping($request->get('conversation_id'), auth()->id()));
    }
}
