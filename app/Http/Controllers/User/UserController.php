<?php

namespace App\Http\Controllers\User;

use App\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getUsersForChat(User $user) {
        return response()->json($user->where('id', '!=', auth()->id())->paginate(5));
    }
}
