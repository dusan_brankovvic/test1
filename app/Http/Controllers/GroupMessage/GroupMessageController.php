<?php

namespace App\Http\Controllers\GroupMessage;

use App\Chat\Message\GroupMessage;
use App\Chat\Message\GroupMessageRepository;
use App\Events\GroupEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupMessageController extends Controller
{
    public function getGroupMessages(GroupMessage $groupMessage)
    {
        return response()->json($groupMessage->all());
    }

    public function newGroupMessage(GroupMessageRepository $groupMessageRepository, GroupMessageNewMessageRequest $newMessageRequest) {
        try {
            $message = $groupMessageRepository->create([
                'sender_id'=>auth()->id(),
                'message'=>$newMessageRequest->json('message')
            ]);

            broadcast(new GroupEvent($message));
            return response()->json(['message'=>$message], 201);

        } catch (\Exception $exception) {
            return response()->json(['message'=>null], 401);
        }


    }
}
