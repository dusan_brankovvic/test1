<?php

namespace App\Http\Controllers\Message;

use App\Chat\Message\MessageRepository;
use App\Events\NewMessage;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function newMessage(NewMessageRequest $newMessageRequest) {
        $message = $this->messageRepository->createNewMessage($newMessageRequest->toArray());

        if( is_null($message)) {
            return response()->json(['message'=>null, 'messageSaved'=>false, 'error'=>'Failed to save message'], 401);

        }
        broadcast(new NewMessage($message));
        return response()->json(['message'=>$message, 'messageSaved'=>true, 'error'=>null], 201);
    }
}
