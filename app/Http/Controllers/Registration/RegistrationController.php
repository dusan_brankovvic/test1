<?php

namespace App\Http\Controllers\Registration;

use App\Services\Authentication\RegistrationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    /**
     * @var RegistrationService
     */
    private $registrationService;

    public function __construct(RegistrationService $registrationService)
    {

        $this->registrationService = $registrationService;
    }

    public function register(RegistrationRequest $registrationRequest) {
        $token = $this->registrationService->register(
            $registrationRequest->get('username'),
            $registrationRequest->get('email'),
            $registrationRequest->get('password')
        );


        if(is_null($token)) {
            return response()->json([], 401);
        }

        return response()->json($token, 201);

    }
}
