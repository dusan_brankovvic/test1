<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function PHPSTORM_META\type;

abstract class RepositoryAbstract implements RepositoryInterface
{
    protected $model;

    public function find(string $id): ?Model
    {
        return $this->model->where('id', $id)->first();
    }

    public function create(array $data): ?Model
    {
        try {
            return $this->model->create($data);
        } catch (\PDOException $exception) {
            dd($exception->getMessage());
            return null;
        }

    }

    public function update(string $id, array $data): bool
    {
        try {
            return $this->model->where('id', $id)->update($data);
        } catch (\PDOException $PDOException) {
            return false;
        }
    }
}
