<?php

namespace App\Chat\Message;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class GroupMessage extends Model
{
    use UsesUuid;

    protected $guarded = ['id'];
}
