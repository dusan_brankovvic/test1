<?php

namespace App\Chat\Message;

use App\RepositoryAbstract;

class MessageRepository extends RepositoryAbstract  {
    protected $model;

    public function __construct(Message $model)
    {
        $this->model = $model;
    }

    public function messageDelivered(string $messageId): bool {
        return $this->model->where('id', $messageId)->update(['delivered'=>1]);
    }

    public function createNewMessage(array $message): ?Message {
        try {
            return $this->model->create($message);
        } catch (\Exception $exception) {
            return null;
        }
    }
}
