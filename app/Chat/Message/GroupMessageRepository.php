<?php

namespace App\Chat\Message;

use App\RepositoryAbstract;

class GroupMessageRepository extends RepositoryAbstract
{
    protected $model;

    public function __construct(GroupMessage $model)
    {
        $this->model = $model;
    }
}
