<?php

namespace App\Chat\Message;

use App\Chat\Conversation\Conversation;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use UsesUuid;

    static $MESSAGE = 0;
    static $PICTURE = 1;
    static $VIDEO = 2;
    protected $guarded = ['id'];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function conversations() {
        return $this->belongsTo(Conversation::class);
    }
}
