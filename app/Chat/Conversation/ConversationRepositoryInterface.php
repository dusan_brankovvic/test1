<?php

namespace App\Chat\Conversation;

use App\User\User;

interface ConversationRepositoryInterface {
    public function createConversationWithMessage(string $creatorId, array $message = []): ?array;

    public function messagesForConversationId(string $conversationId): array;

    public function getConversationsWithFirstMessage(string $userId): array;

    public function createMessageForExistingConversation(string $conversationId, array $message): ?array;

    public function createConversation(string $userId, string $receiver_id): ?array ;
}
