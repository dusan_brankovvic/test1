<?php

namespace App\Chat\Conversation;

use App\RepositoryAbstract;
use App\User\User;
use Illuminate\Support\Facades\Log;

class ConversationRepository extends RepositoryAbstract implements ConversationRepositoryInterface
{
    protected $model;

    static $MESSAGES_PER_PAGE = 1;

    static $CONVERSATIONS_PER_PAGE = 4;

    public function __construct(Conversation $model)
    {
        $this->model = $model;
    }

    public function createConversationWithMessage(string $creatorId, array $message = []): ?array
    {
        try {
            return $this->model->create(['creator_id'=>$creatorId])->messages()->create($message);
        } catch (\Exception $exception) {
            return null;
        }
    }

    public function createMessageForExistingConversation(string $conversationId, array $message): ?array
    {
        try {
            $conversation = $this->model->where('id', $conversationId)->first();

            if (is_null($conversation)) return null;

            return $conversation->messages()->create($message)->toArray();
        } catch (\Exception $exception) {
            return null;
        }
    }

    public function messagesForConversationId(string $conversationId): array
    {
        $conversation = $this->find($conversationId);

        if (is_null($conversation)) return [];

        return $conversation->messages()->orderBy('created_at', 'ASC')->get()->toArray();
    }

    public function getConversationsWithFirstMessage(string $userId): array
    {
        return $this->model->where('creator_id', $userId)->with(['messages' => function ($query) {
            return $query->orderBy('id', 'DESC')->limit(1);
        }])->paginate(self::$CONVERSATIONS_PER_PAGE)->toArray();
    }

    public function createConversation(string $userId, string $receiver_id): ?array
    {
        try {
            $conversation = $this->model->where('creator_id', $userId)->where('receiver_id', $receiver_id)->first();
            if (is_null($conversation)) {
                $conversation = $this->model->where('receiver_id', $userId)->where('creator_id', $receiver_id)->first();
            }
            if($conversation !== null) return $conversation->toArray();
            return $this->model->create(['creator_id'=>$userId, 'receiver_id'=>$receiver_id])->toArray();
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());
            return null;
        }
    }
}
