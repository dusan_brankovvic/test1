<?php

namespace App\Chat\Conversation;

use App\Chat\Message\Message;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use UsesUuid;

    protected $guarded = ['id'];

    public function messages() {
        return $this->hasMany(Message::class);
    }
}
