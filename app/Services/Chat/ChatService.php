<?php

namespace App\Services\Chat;
use App\Chat\Conversation\ConversationRepositoryInterface;
use App\Chat\Message\MessageRepository;

interface ChatServiceInterface {
    public function newMessage(array $message): ?array;
    public function messageDelivered(array $message): bool;
}
class ChatService implements ChatServiceInterface {
    /**
     * @var ConversationRepositoryInterface
     */
    private $conversationRepository;
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(ConversationRepositoryInterface $conversationRepository, MessageRepository $messageRepository)
    {

        $this->conversationRepository = $conversationRepository;
        $this->messageRepository = $messageRepository;
    }

    public function newMessage(array $message): ?array
    {
        if (empty($message) || !isset($message['conversation_id']) || ! isset($message['sender_id'])) return null;

        $createdMessage = $this->conversationRepository->createMessageForExistingConversation($message['conversation_id'], $message);

        if(is_null($createdMessage)) {
            $createdMessage = $this->conversationRepository->createConversationWithMessage($message['sender_id'], $message);
        }

        return $createdMessage;
    }

    public function messageDelivered(array $message): bool
    {
        return $this->messageRepository->messageDelivered($message['id']);
    }
}
