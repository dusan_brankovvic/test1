<?php

namespace App\Services\Authentication;

use App\User\User;
use App\User\UserRepositoryInterface;
use Tymon\JWTAuth\Exceptions\JWTException;

class RegistrationService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    public function register(string $username, string $email, string $password): ?array
    {
        try {
            $user = $this->userRepository->create([
                'username' => $username,
                'email' => $email,
                'password' => $password
            ]);

            $token = auth('api')->login($user);

            return [
                'token' => $token,
                'type' => 'bearer',
                'expires' => auth('api')->factory()->getTTL() * 60,
                'user'=>$user

            ];
        } catch (\Exception $exception) {
            return null;
        }
    }
}
