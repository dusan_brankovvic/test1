<?php

namespace App\Services\Authentication;

use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateService
{
    public function authenticate(string $email, string $password): ?array
    {
        if(!$token = auth('api')->attempt(['email'=>$email, 'password'=>$password])) {
            return null;
        }

        return [
            'token' => $token,
            'type' => 'bearer',
            'expires' => auth('api')->factory()->getTTL() * 60,
            'user'=>auth('api')->user()

        ];
    }

    public function logout(): bool
    {
        try {
            auth('api')->logout();
            return true;
        } catch (JWTException $JWTException) {
            return false;
        }



    }
}
