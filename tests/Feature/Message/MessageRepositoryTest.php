<?php

namespace Tests\Feature\Message;

use App\Chat\Conversation\Conversation;
use App\Chat\Message\Message;
use App\Chat\Message\MessageRepository;
use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MessageRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldUpdateDeliveredMessage() {
        $message = factory(Message::class)->create(['delivered'=>0]);
        $messageRepository = $this->app->make(MessageRepository::class);
        $this->assertTrue($messageRepository->messageDelivered($message->id));

        $this->assertDatabaseHas(Message::getTableName(), ['id'=>$message->id, 'delivered'=>1]);
    }

    public function testItShouldFailedToUpdateMessageForInvalidId() {
        $messageRepository = $this->app->make(MessageRepository::class);
        $this->assertFalse($messageRepository->messageDelivered('1'));

    }

    public function testItShouldCreateNewMessage() {
        $sender = factory(User::class)->create();
        $conversation = factory(Conversation::class)->create(['creator_id'=>$sender->id]);
        $receiver = factory(User::class)->create();
        $messageRepository = $this->app->make(MessageRepository::class);
        $message = $messageRepository->createNewMessage([
            'sender_id'=>$sender->id,
            'message'=>'hello',
            'receiver_id'=>$receiver->id,
            'conversation_id'=>$conversation->id
        ]);

        $this->assertInstanceOf(Message::class, $message);

    }
}
