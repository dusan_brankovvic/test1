<?php

namespace Tests\Feature\Message;

use App\Chat\Conversation\Conversation;
use App\Chat\Message\Message;
use App\Events\NewMessage;
use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MessageControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldCreateNewMessage() {
        $sender = factory(User::class)->create();
        $conversation = factory(Conversation::class)->create(['creator_id'=>$sender->id]);
        $receiver = factory(User::class)->create();

        $this->expectsEvents(NewMessage::class);

        $request = $this->json('POST', route('createNewMessage'), [
            'message'=>'hello',
            'sender_id'=>$sender->id,
            'receiver_id'=>$receiver->id,
            'conversation_id'=>$conversation->id
        ]);

        $request->assertStatus(201);

        $request->assertJsonStructure(['message', 'error', 'messageSaved']);

    }
}
