<?php

namespace Tests\Feature\Services\Chat;

use App\Chat\Conversation\Conversation;
use App\Chat\Message\Message;
use App\Services\Chat\ChatServiceInterface;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatServiceTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateMessageWithoutConversationId()
    {
        $message = factory(Message::class)->make();
        $chatService = $this->app->make(ChatServiceInterface::class);
        $originalMessage = $chatService->newMessage($message->toArray());

        $createdMessage = $originalMessage;

        array_pop($createdMessage);

        $this->assertEquals($message->toArray(), $createdMessage);

        $this->assertDatabaseHas(Message::getTableName(), $originalMessage);
    }

    public function testCreateMessageWithConversationId() {
        $conversation = factory(Conversation::class)->create();
        $message = factory(Message::class)->make(['conversation_id'=>$conversation->id]);

        $chatService = $this->app->make(ChatServiceInterface::class);
        $originalMessage = $chatService->newMessage($message->toArray());

        $createdMessage = $originalMessage;

        array_pop($createdMessage);

        $this->assertEquals($message->toArray(), $createdMessage);

        $this->assertDatabaseHas(Message::getTableName(), $originalMessage);

    }

    public function testCreateMessageWithInvalidData() {
        $chatService = $this->app->make(ChatServiceInterface::class);
        $this->assertNull($chatService->newMessage([]));
    }

    public function testCreateMessageWithNotFullArray() {
        $chatService = $this->app->make(ChatServiceInterface::class);
        $this->assertNull($chatService->newMessage(['conversation_id'=>'1', 'sender_id'=>'1']));
    }
}
