<?php

namespace Tests\Feature\Conversation;

use App\Chat\Conversation\Conversation;
use App\Chat\Message\Message;
use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConversationControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldReturnConversationsWithMessage()
    {
        $user = $this->login();

        $conversation = factory(Conversation::class)->create(['creator_id'=>$user->id]);

        $message = factory(Message::class)->create(['conversation_id'=>$conversation->id]);

        $request = $this->json('GET', route('getConversationsWithSingleMessage'));

        $request->assertStatus(200);

        $request->assertJsonStructure(['data']);

        $this->assertCount(1, $request->json('data'));
    }

    public function testItShouldCreateConversation()
    {
        $user = $this->login();
        $userReceiver = factory(User::class)->create();
        $request = $this->json('POST', route('createConversation'), ['receiver_id'=>$userReceiver->id]);

        $request->assertStatus(201);

        $request->assertJsonStructure(['conversation']);

        $this->assertNotNull($request->json('conversation'));

    }

    public function testItShouldFetchMessagesForConversationId() {
        $conversation = factory(Conversation::class)->create();
        $messages = factory(Message::class, 10)->create(['conversation_id'=>$conversation->id]);

        $request = $this->json('GET', route('getMessagesForConversationId'),
            ['conversation_id'=>$conversation->id]);

        $request->assertStatus(200);

        $this->assertEquals($request->json(), $messages->toArray());
    }
}
