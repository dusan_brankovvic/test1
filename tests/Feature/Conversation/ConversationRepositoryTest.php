<?php

namespace Tests\Feature\Conversation;

use App\Chat\Conversation\Conversation;
use App\Chat\Conversation\ConversationRepositoryInterface;
use App\Chat\Message\Message;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConversationRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldGetMessagesForConversationId()
    {
        $conversation = factory(Conversation::class)->create();
        $messages = factory(Message::class, 10)->create(['conversation_id'=>$conversation->id]);
        $conversationRepository = $this->app->make(ConversationRepositoryInterface::class);
        $messagesFromRepo = $conversationRepository->messagesForConversationId($conversation->id);
        $this->assertEquals($messagesFromRepo, $messages->toArray());
    }
}
