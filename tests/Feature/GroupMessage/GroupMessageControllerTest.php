<?php

namespace Tests\Feature\GroupMessage;

use App\Chat\Message\GroupMessage;
use App\Events\GroupEvent;
use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupMessageControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldCreateNewGroupMessage() {
        $this->login();

        $this->expectsEvents(GroupEvent::class);

        $request = $this->json('POST', route('newGroupMessage'), [
            'message'=>'yea'
        ]);

        $request->assertStatus(201);

        $this->assertEquals('yea', $request->json('message')['message']);

    }

    public function testItShouldGetGroupMessages()
    {
        $user = $this->login();

        factory(GroupMessage::class, 4)->create(['sender_id'=>$user->id]);

        $request = $this->json('GET', route('getGroupMessages'));

        $request->assertStatus(200);

        $this->assertCount(4, $request->json());
    }
}
