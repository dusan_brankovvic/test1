<?php

namespace Tests\Feature\GroupMessage;

use App\Chat\Message\GroupMessageRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupMessageRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldCreateGroupMessage()
    {
        $user = $this->login();

        $groupMessageRepository = $this->app->make(GroupMessageRepository::class);

        $message = $groupMessageRepository->create([
            'sender_id'=>$user->id,
            'message'=>'yea'
        ]);

        $this->assertEquals($message['message'], 'yea');

    }
}
