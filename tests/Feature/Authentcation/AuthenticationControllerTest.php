<?php

namespace Tests\Feature\Authentcation;

use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldAuthenticateUser()
    {
        $user = factory(User::class)->create(['password'=>'test']);

        $request = $this->json('POST', route('authenticate'), ['email'=>$user->email, 'password'=>'test']);

        $request->assertStatus(201);

        $request->assertJsonStructure(['token', 'type', 'expires', 'user']);
    }

    public function testItShouldLogoutUser()
    {
        $user = factory(User::class)->create(['password'=>'test']);

        $request = $this->json('POST', route('authenticate'), ['email'=>$user->email, 'password'=>'test']);

        $request->assertStatus(201);

        $logoutRequest = $this->json('POST', route('logout'));

        $logoutRequest->assertStatus(201);
    }
}
