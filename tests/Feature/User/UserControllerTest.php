<?php

namespace Tests\Feature\User;

use App\Chat\Conversation\Conversation;
use App\Events\NewMessage;
use App\User\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldFetUserForConversation() {
        $sender = factory(User::class)->create();
        $receiver = factory(User::class)->create();

        $request = $this->json('GET', route('getUsersForChat'));

        $request->assertStatus(200);

        $request->assertJsonStructure(['data']);

    }
}
