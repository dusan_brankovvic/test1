<?php

namespace Tests;

use App\User\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function login(User $user = null): User {
        $user = is_null($user) ? factory(User::class)->create() : $user;
        $this->actingAs($user, 'api');
        return $user;
    }
}
